function [Simul, bff_misestimationfactor, IndCO_out] = CorrectCO(Obs,Simul,figdisplay)

if ~exist('figdisplay','var')
    figdisplay = 0;
end

Eup = zeros(size(Simul));
IndCO = zeros(size(Simul));

Table_Line = {'CO 3-2','CO 4-3','CO 6-5','CO 7-6','CO 10-9',...
    'CO 15-14','CO 17-16','CO 19-18'};
Table_Eup = [33.2 55.3 116.2 154.87 304.17 663.36 845.61 1049.85];

for it=1:length(Table_Eup);
    ind = find(strcmpi(Obs(:,2),Table_Line{it}));
    IndCO(ind) = 1;
    Eup(ind) = Table_Eup(it);
end

% ADDED
IndCO_out = IndCO;
%
IndCO = find(IndCO);
bff_factor = cell2mat(Obs(:,7))./Simul;
if figdisplay == 1
    figure;
    plot(Eup(IndCO),bff_factor(IndCO),'o')
end

factor = fittype( @(factor0,E0,x) factor0*exp(-x/E0)+1 );
opt = fitoptions(factor);
opt.Lower = [0 0];
opt.Upper = [2*max(bff_factor(IndCO)) max(Eup(:))];
opt.StartPoint = [max(bff_factor(IndCO)) max(bff_factor(:))/4];
fitresult = fit(Eup(IndCO),bff_factor(IndCO),factor,opt);

if figdisplay == 1
    Eupmod = 0:1200;
    factormod = factor(fitresult.factor0,fitresult.E0,Eupmod);
    hold on
    plot(Eupmod,factormod,'r')
    fs = 13;
    xlabel('Eup (K)','fontsize',fs)
    ylabel('Factor of BFF underestimation','fontsize',fs)
    set(gca,'fontsize',fs)
end

Simul(IndCO) = Simul(IndCO).*...
    factor(fitresult.factor0,fitresult.E0,Eup(IndCO));

bff_misestimationfactor = factor(fitresult.factor0,fitresult.E0,Eup(IndCO));

end
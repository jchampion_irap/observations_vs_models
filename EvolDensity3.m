%% Cleaning workspace
clear all
close all
clc

Lines = {'CO(3-2)';'CO(4-3)';'CO(6-5)';'CO(7-6)';...
    'CO(10-9)';'CO(15-14)';'CO(17-16)';'CO(19-18)';...
    '13CO(3-2)';'[CII]';'[OI]';'HCO+(4-3)';'HCN(4-3)'}; % Line names.
Instru = {'APEX';'APEX';'APEX';'APEX | HIFI';'HIFI';...
    'PACS';'PACS';'PACS';'APEX';'HIFI';'PACS';'APEX';'APEX'};
lambda0 = ones(size(Lines))*NaN;
thetab = ones(size(Lines))*NaN;
LineFlux_Kkmpers = ones(size(Lines))*NaN;
Int = ones(length(Lines),3)*NaN;
Int_corr = ones(length(Lines),3)*NaN;

k = 1.38065E-23; % Boltzmann constant in J/K.
c = 299792458; % Light velocity in vaccum in m/s.

%% Correcting observed fluxes
% Data from Sahai et al. (2012)
% Sahai R., Güsten R. and Morris M. R. (2012), Are large, cometary-shaped
% proplyds really evaporating gas globules?. ApJL (761:L21).
ind = strfind(Instru,'APEX');
ind = ~cellfun(@isempty,ind);
% ind = strcmpi(Instru,'APEX');
lambda0(ind) = [866.963;650.251;433.556;371.650;906.830;840.407;845.665]*1E-6;
thetab(ind) = [17.8;13.1;8.7;7.7;18.6;17.3;17.4]; % Beam sizes (FWHM) in arcsec.
LineFlux_Kkmpers(ind) = [3.5;5.4;7.3;8.1;0.96;0.37;0.21]; % Line fluxes in K.km/s.
% Conversion to SI units.
Int(:,1) = (2E3*k*LineFlux_Kkmpers)./(lambda0.^3); % Int. or line fluxes in W/m²/sr.
Senv = pi*(9.5/2)*(3.7/2); % Surface in arcseconds² of the Carina proplyd disk.
Sbeam = pi*(thetab/2).^2; % Surface in arcseconds² of the beam.
bff = Senv./Sbeam; % Beam filling factor.
Int_corr(:,1) = Int(:,1)./bff; % Intensity corrected with the beam filling factor.

% Data from our analyses with PACS.
ind = strfind(Instru,'PACS');
ind = ~cellfun(@isempty,ind);
% ind = strcmpi(Instru,'PACS');
lambda0(ind) = [173.631;153.267;137.196;63.184]*1E-6;
Int(ind,2) = [2E-9;2.76E-9;NaN;1.24E-6]; % Line fluxes in W/m²/sr.
Sbeam(ind) = 9.4^2; % Pixel of 9.4 by 9.4 arcsec².
bff = Senv./Sbeam; % Beam filling factor.
Int_corr(:,2) = Int(:,2)./bff;

% Data from our analyses with HIFI.
ind = strfind(Instru,'HIFI');
ind = ~cellfun(@isempty,ind);
% ind = strcmpi(Instru,'HIFI');
lambda0(ind) = [371.650;260.240;157.68]*1E-6;
f0 = c./lambda0;
Int(ind,3) = [4.79E-10;1.57E-9;1.29E-7]; % Line fluxes in W/m²/sr.
Table = [480 44.3; 640 33.2; 800 26.6; 960 22.2; 1120 19; 1410 15.2;...
    1910 11.2]; % Frequency (GHz) vs FWHM (arcsec.), from HIFI observer manual.
% Calculation of the FWHM (frequency dependent) for each HIFI observation,
% and thus the beam aperture.
for i = find(ind(:)'==1)
    [~, indi] = min(abs(Table(:,1)-f0(i)*1E-9));
    FWHM = Table(indi,2) * Table(indi,1)/(f0(i)*1E-9); % Because f1/f2 = FWHM2/FWHM1.
    Sbeam(i) = pi * (FWHM/2)^2;
end
bff = Senv./Sbeam; % Beam filling factor.
Int_corr(:,3) = Int(:,3)./bff;

%% Simulation
% filepath = '/home/champion/Model/1.4.4/out/CARINA_expprofil_Av38/lines_light';
filepath = '/home/champion/Model/1.4.4/out/CARINA_1e-2_Av1_O10x_1600/lines20_light';
simul = ReadSimul(filepath);

%% Figure
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])

mks = 5;
semilogy(Int_corr(:,1),'o','MarkerSize',mks,'MarkerFaceColor','r',...
    'MarkerEdgeColor','r')
hold on
semilogy(Int_corr(:,2),'s','MarkerSize',mks,'MarkerFaceColor','g',...
    'MarkerEdgeColor','g')
semilogy(Int_corr(:,3),'d','MarkerSize',mks,'MarkerFaceColor','b',...
    'MarkerEdgeColor','b')
xticklabel = cell(size(Lines));
for it=1:length(Lines)
    xticklabel{it} = [Lines{it} char(10) Instru{it}];
end
axe = axis;
axe(1:2) = [0 length(Lines)+1];
axis(axe);
my_xticklabels(gca,1:length(Lines),xticklabel,'Rotation',90,...
    'HorizontalAlignment','Right','VerticalAlignment','Middle',...
    'fontsize',8);
fs = 13;
ylabel('Line intensities (W/m²/sr)','fontsize',fs)
set(gca,'fontsize',fs)

mks = 5;
semilogy(simul,'s','MarkerSize',mks,'MarkerEdgeColor','k')

% axe(3:4) = [1e-15 1e-5];
% axis(axe)
% yt = [1e-14:1e-14:1e-13 ...
%     2e-13:1e-13:1e-12 2e-12:1e-12:1e-11 2e-11:1e-11:1e-10 ...
%     2e-10:1e-10:1e-09 2e-09:1e-09:1e-08 2e-08:1e-08:1e-07 ...
%     2e-07:1e-07:1e-06 2e-06:1e-06:1e-05];
% set(gca,'YTick',yt)
% ytl = cell(1,length(yt));
% for i=1:9:length(yt)
%     ytl{i}=yt(i);
% end
% set(gca,'YTickLabel',ytl);
set(gca,'YGrid','on')

legend('Corrected APET Obs.','Corrected PACS Obs.',...
    'Corrected HIFI Obs.','Simulation','Location','NorthWest')

print('-dpng','-r500','EvolDensity3.png')

%% Figure 2
fact = Int_corr./Int;
simul_corr(:,1) = simul./fact(:,1);
simul_corr(:,2) = simul./fact(:,2);
simul_corr(:,3) = simul./fact(:,3);
simul_corr(8,2) = simul(8)*Senv/(9.4^2);

figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])

mks = 5;
semilogy(Int(:,1),'o','MarkerSize',mks,'MarkerFaceColor','r',...
    'MarkerEdgeColor','r')
hold on
semilogy(Int(:,2),'s','MarkerSize',mks,'MarkerFaceColor','g',...
    'MarkerEdgeColor','g')
semilogy(Int(:,3),'d','MarkerSize',mks,'MarkerFaceColor','b',...
    'MarkerEdgeColor','b')
xticklabel = cell(size(Lines));
for it=1:length(Lines)
    xticklabel{it} = [Lines{it} char(10) Instru{it}];
end
axe = axis;
axe(1:2) = [0 length(Lines)+1];
axis(axe);
my_xticklabels(gca,1:length(Lines),xticklabel,'Rotation',90,...
    'HorizontalAlignment','Right','VerticalAlignment','Middle',...
    'fontsize',8);
fs = 13;
ylabel('Line intensities (W/m²/sr)','fontsize',fs)
set(gca,'fontsize',fs)

mks = 5;
semilogy(simul_corr,'s','MarkerSize',mks,'MarkerEdgeColor','k')

% axe(3) = [1e-15];
% axis(axe)
% yt = [1e-14:1e-14:1e-13 ...
%     2e-13:1e-13:1e-12 2e-12:1e-12:1e-11 2e-11:1e-11:1e-10 ...
%     2e-10:1e-10:1e-09 2e-09:1e-09:1e-08 2e-08:1e-08:1e-07 ...
%     2e-07:1e-07:1e-06 2e-06:1e-06:1e-05];
% set(gca,'YTick',yt)
% ytl = cell(1,length(yt));
% for i=1:9:length(yt)
%     ytl{i}=yt(i);
% end
% set(gca,'YTickLabel',ytl);
set(gca,'YGrid','on')

legend('APEX Obs.','PACS Obs.',...
    'HIFI Obs.','Corrected simulation','Location','NorthWest')

print('-dpng','-r500','EvolDensity3_v2.png')
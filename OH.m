clear all
close all
clc

addpath('../Observations/Herschel/Flux/');
LineFluxes = GetLineFluxes(0);
IndObs = find(strcmpi(LineFluxes(:,2),'OH') & ...
    strcmpi(LineFluxes(:,5),'244-440'));
Flux = cell2mat(LineFluxes(IndObs,7));
Uncert = cell2mat(LineFluxes(IndObs,8));
filepaths = {'/home/champion/Model/1.4.4/out/CARINA_5e5_Av5_500K/linesOH_light',...
    '/home/champion/Model/1.4.4/out/CARINA_1e6_Av5_500K_corr/linesOH_light',...
    '/home/champion/Model/1.4.4/out/CARINA_5e6_Av5_500K_corr/linesOH_light',...
    '/home/champion/Model/1.4.4/out/CARINA_1e7_Av5_500K_corr_2/linesOH_light',...
    '/home/champion/Model/1.4.4/out/CARINA_5e7_Av5_4000K_corr/linesOH_light'};
descr = {'isoch. 5e5','isoch. 1e6','isoch. 5e6','isoch. 1e7','isoch. 5e7'};
n = [5e5 1e6 5e6 1e7 5e7];
filepaths2 = {'/home/champion/Model/1.4.4/out/CARINA_isobar_3e8/linesOH_light',...
    '/home/champion/Model/1.4.4/out/CARINA_isobar_6e8/linesOH_light',...
    '/home/champion/Model/1.4.4/out/CARINA_isobar_1e9/linesOH_light',...
    '/home/champion/Model/1.4.4/out/CARINA_isobar_5e9/linesOH_light'};
press = [3e8 6e8 1e9 5e9];
nfiles = length(filepaths);
nfiles2 = length(filepaths2);
simul = zeros(nfiles,1);
simul2 = zeros(nfiles2,1);
for it = 1:nfiles
    simul(it,1) = sum(ReadSimul(filepaths{it}));
end
for it = 1:nfiles2
    simul2(it,1) = sum(ReadSimul(filepaths2{it}));
end

D = 0.4e3; % Distance in parsec.
diam = 387; % AU
diam = 3600*atand(diam*tand(1/3600)/D);
simul = simul*(pi*(diam/2)^2)/(9.4^2);
p1 = plot(n,simul,'ko','markerfacecolor','k');
nmod = logspace(log10(n(1)),log10(n(end)),10000);
% [Coeffs, S] = polyfit(n(:),simul(:),1);
% simulmod = polyval(Coeffs,nmod,S);
% %{
% % fct = fittype( @(a,b,x) a*exp(log(x).^2)+b );
% % opt = fitoptions(fct);
% % opt.Lower = [0 -Inf];
% % opt.Upper = [Inf Inf];
% % opt.StartPoint = [1 0];
% % fitresult = fit(n(:),simul(:),fct,opt);
% % hold on
% % plot(fitresult)
% %}
% hold on
% plot(nmod,simulmod,'r')
% set(gca,'XScale','log')
% set(gca,'YScale','log')
simulmod = interp1(n(:),simul(:),nmod,'cubic');
hold on
p2 = plot(nmod,simulmod,'r','linewidth',2);
set(gca,'XScale','log')
set(gca,'YScale','log')
axe = axis;
axe(3)=1e-10;
axis(axe);

Fmin = Flux*(1-Uncert/100);
Fmax = Flux*(1+Uncert/100);
p3 = patch([axe(1:2) fliplr(axe(1:2))],[Fmin Fmin Fmax Fmax],[0.8 0.8 1]);
alpha(p3,0.5)
nmin = nmod(find(simulmod<Fmin,1,'last'));
nmax = nmod(find(simulmod>Fmax,1,'first'));
line([nmin nmin],[axe(3) Fmin],'color','k','linestyle','--')
line([nmax nmax],[axe(3) Fmax],'color','k','linestyle','--')
nminstr = num2str(nmin,'%9.2e');
nmaxstr = num2str(nmax,'%9.2e');
fs = 13;
text(nmin,axe(3),['    ' nminstr],'HorizontalAlignment','left',...
    'VerticalAlignment','Bottom','Rotation',90,'fontweight','bold',...
    'fontSize',fs)
text(nmax,axe(3),['    ' nmaxstr],'HorizontalAlignment','left',...
    'VerticalAlignment','Top','Rotation',90,'fontweight','bold',...
    'fontSize',fs)
set(gca,'fontSize',fs)
xlabel('n_H (cm^{-3})','fontSize',fs)
ylabel('OH line flux (W/m²/sr)','fontSize',fs)

legend([p1 p2 p3],{'Simulated and bff corrected','Cubic interpolation',...
    'Observed'},'location','NorthWest')

print('-dpng','-r500','OH_244-440.png')

%%
figure
simul2 = simul2*(pi*(diam/2)^2)/(9.4^2);
p2_1 = plot(press,simul2,'ko','markerfacecolor','k');
pressmod = logspace(log10(press(1)),log10(press(end)),10000);
simulmod2 = interp1(press(:),simul2(:),pressmod,'cubic');
hold on
p2_2 = plot(pressmod,simulmod2,'r','linewidth',2);
set(gca,'XScale','log')
set(gca,'YScale','log')
axe = axis;
axe(3)=1e-10;
axis(axe);

Fmin = Flux*(1-Uncert/100);
Fmax = Flux*(1+Uncert/100);
p2_3 = patch([axe(1:2) fliplr(axe(1:2))],[Fmin Fmin Fmax Fmax],[0.8 0.8 1]);
alpha(p2_3,0.5)
pressmin = pressmod(find(simulmod2<Fmin,1,'last'));
pressmax = pressmod(find(simulmod2>Fmax,1,'first'));
line([pressmin pressmin],[axe(3) Fmin],'color','k','linestyle','--')
line([pressmax pressmax],[axe(3) Fmax],'color','k','linestyle','--')
pressminstr = num2str(pressmin,'%9.2e');
pressmaxstr = num2str(pressmax,'%9.2e');
fs = 13;
text(pressmin,axe(3),['    ' pressminstr],'HorizontalAlignment','left',...
    'VerticalAlignment','Bottom','Rotation',90,'fontweight','bold',...
    'fontSize',fs)
text(pressmax,axe(3),['    ' pressmaxstr],'HorizontalAlignment','left',...
    'VerticalAlignment','Top','Rotation',90,'fontweight','bold',...
    'fontSize',fs)
set(gca,'fontSize',fs)
xlabel('Pressure (cm^{-3}.K)','fontSize',fs)
ylabel('OH line flux (W/m²/sr)','fontSize',fs)

legend([p2_1 p2_2 p2_3],{'Simulated and bff corrected','Cubic interpolation',...
    'Observed'},'location','NorthWest')

print('-dpng','-r500','OH_244-440_2.png')
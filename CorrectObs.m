function CorrectedObs = CorrectObs(Obs,semimajoraxis_as,semiminoraxis_as)

CorrectedObs = Obs;
ObsInt = zeros(size(Obs,1),1);
Ind_CircularBeam = ~cellfun(@isempty,strfind(Obs(:,1),'APEX')) | ...
    ~cellfun(@isempty,strfind(Obs(:,1),'HIFI'));
Ind_SquareBeam = ~cellfun(@isempty,strfind(Obs(:,1),'PACS'));

Sobj = pi*semimajoraxis_as*semiminoraxis_as;
ObsInt(Ind_CircularBeam) = ...
    cell2mat(Obs(Ind_CircularBeam,7)) .* ...
    (pi*(cell2mat(Obs(Ind_CircularBeam,4))/2).^2)./Sobj;
ObsInt(Ind_SquareBeam) = ...
    cell2mat(Obs(Ind_SquareBeam,7)) .* ...
    (cell2mat(Obs(Ind_SquareBeam,4)).^2)./Sobj;

CorrectedObs(:,7) = mat2cell(ObsInt,ones(size(Obs,1),1));

end
function simul = ReadSimul(filepath,ind)

data = importdata(filepath);

if strcmpi(filepath(end-4:end),'_corr')  
    if ~exist('ind','var')
        ind = [1 10 2 3 5 4 6 7 8 9 11 12 13 14];
    end
    simul = data.data(ind);
else
    simul = zeros(size(data));
    for i=1:length(data)
        str = data{i};
        str = str(end-10:end);
        simul(i) = str2double(str);
    end
    if exist('ind','var')
        simul = simul(ind);
    end
end

end
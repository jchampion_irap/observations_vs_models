function [CorrectedSimul, bff] = CorrectSimul(Obs,Simul,semimajoraxis_as,semiminoraxis_as)

CorrectedSimul = Simul;
Ind_CircularBeam = ~cellfun(@isempty,strfind(Obs(:,1),'APEX')) | ...
    ~cellfun(@isempty,strfind(Obs(:,1),'HIFI'));
Ind_SquareBeam = ~cellfun(@isempty,strfind(Obs(:,1),'PACS'));

Sobj = pi*semimajoraxis_as*semiminoraxis_as;
if ~isempty(find(Ind_CircularBeam==1,1))
    CorrectedSimul(Ind_CircularBeam) = ...
        Simul(Ind_CircularBeam) .* ...
        Sobj./(pi*(cell2mat(Obs(Ind_CircularBeam,4))/2).^2);
    % ADDED
    bff(Ind_CircularBeam) = Sobj./(pi*(cell2mat(Obs(Ind_CircularBeam,4))/2).^2);
    %
end
if ~isempty(find(Ind_SquareBeam==1,1))
    CorrectedSimul(Ind_SquareBeam) = ...
        Simul(Ind_SquareBeam) .* ...
        Sobj./(cell2mat(Obs(Ind_SquareBeam,4)).^2);
    % ADDED
    bff(Ind_SquareBeam) = Sobj./(cell2mat(Obs(Ind_SquareBeam,4)).^2);
    %
end

end
%% Cleaning workspace
clear all
close all
clc

%% Figure
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])

%% Observation data
Lines = {'CO(3-2)';'13CO(3-2)';'CO(4-3)';'CO(6-5)';'CO(7-6)';...
    'CO(7-6)';'CO(10-9)';'CO(15-14)';'CO(17-16)';'CO(19-18)';...
    '[CII]';'[OI]';'HCO+(4-3)';'HCN(4-3)'}; % Line names.
Instru = {'APET';'APET';'APET';'APET';'APET';'HIFI';'HIFI';...
    'PACS';'PACS';'PACS';'HIFI';'PACS';'APET';'APET'};
Obs = [1.48E-10;3.55E-11;5.42E-10;2.47E-09;4.36E-09;...
    4.79E-10;1.57E-09;...
    2E-09;2.76E-09;NaN;...
    1.29E-07;...
    1.24E-06;...
    1.72E-11;...
    9.59E-12];

mks = 5;
semilogy(Obs,'o','MarkerSize',mks,'MarkerFaceColor','k',...
    'MarkerEdgeColor','k')
xticklabel = cell(size(Lines));
for it=1:length(Lines)
    xticklabel{it} = [Lines{it} char(10) Instru{it}];
end
% set(gca,'XTickLabel',xticklabel)
axe = axis;
axe(1:2) = [0 length(Lines)+1];
axis(axe);
my_xticklabels(gca,1:length(Lines),xticklabel,'Rotation',90,...
    'HorizontalAlignment','Right','VerticalAlignment','Middle',...
    'fontsize',8);
fs = 13;
ylabel('Line intensities (W/m²/sr)','fontsize',fs)
set(gca,'fontsize',fs)

%% Model data
filepath = '/home/champion/Model/1.4.4/out/CARINA_default_Av10_O10x/lines_light_corr';
simul_1e5 = ReadSimul(filepath);
filepath = '/home/champion/Model/1.4.4/out/CARINA_default_Av10_O10x_1e6/lines_light_corr';
simul_1e6 = ReadSimul(filepath);
filepath = '/home/champion/Model/1.4.4/out/CARINA_default_Av10_O10x_5e6/lines_light_corr';
simul_5e6 = ReadSimul(filepath);
filepath = '/home/champion/Model/1.4.4/out/CARINA_default_Av10_O10x_1e7/lines_light_corr';
simul_1e7 = ReadSimul(filepath);
hold on
mks = 2;
semilogy(simul_1e5,'s','MarkerSize',mks,'MarkerFaceColor','b',...
    'MarkerEdgeColor','b')
mks = 3;
semilogy(simul_1e6,'s','MarkerSize',mks,'MarkerFaceColor','m',...
    'MarkerEdgeColor','m')
mks = 4;
semilogy(simul_5e6,'s','MarkerSize',mks,'MarkerFaceColor','g',...
    'MarkerEdgeColor','g')
mks = 5;
semilogy(simul_1e7,'s','MarkerSize',mks,'MarkerFaceColor','r',...
    'MarkerEdgeColor','r')

legend('Observation','Simulation (1e5)','Simulation (1e6)',...
    'Simulation (5e6)','Simulation (1e7)','Location','NorthWest')
% axe(3:4) = [1e-16 1e-5];
% axis(axe)
% yt = [1e-16:1e-16:1e-15 2e-15:1e-15:1e-14 2e-14:1e-14:1e-13 ...
%     2e-13:1e-13:1e-12 2e-12:1e-12:1e-11 2e-11:1e-11:1e-10 ...
%     2e-10:1e-10:1e-09 2e-09:1e-09:1e-08 2e-08:1e-08:1e-07 ...
%     2e-07:1e-07:1e-06 2e-06:1e-06:1e-05];
yt = [1e-12:1e-12:1e-11 2e-11:1e-11:1e-10 ...
    2e-10:1e-10:1e-09 2e-09:1e-09:1e-08 2e-08:1e-08:1e-07 ...
    2e-07:1e-07:1e-06 2e-06:1e-06:1e-05];
set(gca,'YTick',yt)
ytl = cell(1,length(yt));
for i=1:9:length(yt)
    ytl{i}=yt(i);
end
set(gca,'YTickLabel',ytl);
set(gca,'YGrid','on')
% print('-dpng','-r500','EvolDensity_Extended.png')
print('-dpng','-r500','EvolDensity.png')
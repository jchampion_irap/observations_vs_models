clear all
close all
clc

%% Observations
% Get observations.
CarinaObs = GetCarinaObs();

% Correct observations.
% disk_a = 0.83/2;
% disk_c = 0.4/2;
% CarinaObs = CorrectObs(CarinaObs,disk_a,disk_c);

% Plot observations.
fig1 = figure;
set(fig1,'units','normalized','position',[0 0 1 1])
hold on
mks_obs = 5;
% indAPEX = strfind(CarinaObs(:,1),'APEX');
% indAPEX = ~cellfun(@isempty,indAPEX);
% X = find(indAPEX);
% errorbar(X,cell2mat(CarinaObs(indAPEX,7)),...
%     0.01*cell2mat(CarinaObs(indAPEX,8)).*cell2mat(CarinaObs(indAPEX,7)),...
%     'ro','MarkerSize',mks_obs,...
%     'MarkerFaceColor','r','MarkerEdgeColor','r')
% indHIFI = strfind(CarinaObs(:,1),'HIFI');
% indHIFI = ~cellfun(@isempty,indHIFI);
% X = find(indHIFI);
% errorbar(X,cell2mat(CarinaObs(indHIFI,7)),...
%     0.01*cell2mat(CarinaObs(indHIFI,8)).*cell2mat(CarinaObs(indHIFI,7)),...
%     'gd','MarkerSize',1.5*mks_obs,...
%     'MarkerFaceColor','g','MarkerEdgeColor','g')
% indPACS = strfind(CarinaObs(:,1),'PACS');
% indPACS = ~cellfun(@isempty,indPACS);
% X = find(indPACS);
% errorbar(X,cell2mat(CarinaObs(indPACS,7)),...
%     0.01*cell2mat(CarinaObs(indPACS,8)).*cell2mat(CarinaObs(indPACS,7)),...
%     'bs','MarkerSize',mks_obs,...
%     'MarkerFaceColor','b','MarkerEdgeColor','b')
% set(gca,'YScale','log')
% legend('APEX','HIFI','PACS',...
%     'location','NorthWest')
indFlux = strcmpi(CarinaObs(:,6),'Flux');
X = find(indFlux);
p1 = errorbar(X,cell2mat(CarinaObs(indFlux,7)),...
    0.01*cell2mat(CarinaObs(indFlux,8)).*cell2mat(CarinaObs(indFlux,7)),...
    'ko','MarkerSize',mks_obs,...
    'MarkerFaceColor','k','MarkerEdgeColor','k');
indSupFlux = strfind(CarinaObs(:,6),'SupFlux');
indSupFlux = ~cellfun(@isempty,indSupFlux);
X = find(indSupFlux);
p2 = plot(X,cell2mat(CarinaObs(indSupFlux,7))+...
    0.01*cell2mat(CarinaObs(indSupFlux,8)).*...
    cell2mat(CarinaObs(indSupFlux,7)),...
    'kv','MarkerSize',mks_obs,...
    'MarkerFaceColor','k','MarkerEdgeColor','k');
set(gca,'YScale','log')
legend('Observations','Non detection',...
    'location','NorthWest')

%% Simulation
indSimul = [1:4 4 5:9 13 12 10 11];
datapath = '/home/champion/Model/1.4.4/out/'; % PC bureau
if exist(datapath,'dir')~=7
    datapath = '/home/jason/Documents/Stage/Codes/ModelOut/'; % Portable Jason
end
filepaths = {...
    [datapath 'CARINA_5e5_Av5_500K/lines_light'];...
    [datapath 'CARINA_1e6_Av5_500K_corr/lines_light'];...
    [datapath 'CARINA_5e6_Av5_500K_corr/lines_light'];...
    [datapath 'CARINA_1e7_Av5_500K_corr_2/lines_light'];...
    [datapath 'CARINA_5e7_Av5_4000K_corr/lines_light'];...
    [datapath 'CARINA_isobar_3e8/lines_light'];...
    [datapath 'CARINA_isobar_6e8/lines_light'];...
    [datapath 'CARINA_isobar_1e9/lines_light'];...
    [datapath 'CARINA_isobar_5e9/lines_light'];...
    [datapath 'CARINA_ENV_n2750/lines_light'];...
    [datapath 'CARINA_ENV_n13000/lines_light'];...
    [datapath 'CARINA_ENV_n15000/lines_light']};
modeldescriptions = {...
    'isoch. 5e5';...
    'isoch. 1e6';...
    'isoch. 5e6';...
    'isoch. 1e7';...
    'isoch. 5e7';...
    'isobar 3e8';...
    'isobar 6e8';...
    'isobar 1e9';...
    'isobar 5e9';...
    'Env. @ 2750';...
    'Env. @ 13000';...
    'Env. @ 15000'};
nfiles = size(filepaths,1);
isdisk = ones(1,nfiles);
ind = strfind(modeldescriptions,'Env');
ind = find(~cellfun(@isempty,ind));
isdisk(ind) = 0;
nisoch = length(cell2mat(strfind(modeldescriptions,'isoch')));
nisob = length(cell2mat(strfind(modeldescriptions,'isob')));
nenv = length(cell2mat(strfind(modeldescriptions,'Env')));
nscalecol = 10;
mkcisoc = hot(nisoch*nscalecol);
mkcisoc = mkcisoc(fix(0.2*nisoch*nscalecol:0.6*nisoch*nscalecol/(nisoch-1):0.8*nisoch*nscalecol),:);
mkcisob = winter(nisob);
mkcenv = cool(nenv*nscalecol);
mkcenv = mkcenv(fix(0.2*nenv*nscalecol:0.6*nenv*nscalecol/(nenv-1):0.8*nenv*nscalecol),:);
mkc = [mkcisoc;mkcisob;mkcenv];
onlyisoch = 0;
onlyisob = 0;
onlyenv = 1;
correctornot = 0;
if onlyisoch == 1
    ind = strfind(modeldescriptions,'isoch');
    ind = find(~cellfun(@isempty,ind));
    filepaths = filepaths(ind);
    modeldescriptions = modeldescriptions(ind);
    isdisk = isdisk(ind);
    mkc = mkc(ind,:);
elseif onlyisob == 1
    ind = strfind(modeldescriptions,'isob');
    ind = find(~cellfun(@isempty,ind));
    filepaths = filepaths(ind);
    modeldescriptions = modeldescriptions(ind);
    isdisk = isdisk(ind);
    mkc = mkc(ind,:);
elseif onlyenv == 1
    ind = strfind(modeldescriptions,'Env');
    ind = find(~cellfun(@isempty,ind));
    filepaths = filepaths(ind);
    modeldescriptions = modeldescriptions(ind);
    isdisk = isdisk(ind);
    mkc = mkc(ind,:);
end
nfiles = size(filepaths,1);

plothandles = [p1 p2];
lstr = {'Observations (bff from H2)','Non detection (bff from H2)'};
mkt = 's';
mks = 5;
X = 1:size(CarinaObs,1);
Simul = zeros(size(CarinaObs,1),nfiles);
fs = 13;

% ADDED
XLSOUT = CarinaObs;
XLSOUT(:,5) = [];
if onlyisoch==1
    indii = 5;
    XLSOUTHEAD = {'Instrument' 'Line' 'Wavelength (um)' 'FWHM (")' 'Type of Value' ...
        'Value (W/m²/sr)' 'Uncertainty (%)' 'Modeled value' 'bff based on H2' ...
        'Modeled value / geometry based on H2' 'bff empiric multiplication factor' ...
        'Final modeled value'};
elseif onlyenv==1
    indii = 2;
    XLSOUTHEAD = {'Instrument' 'Line' 'Wavelength (um)' 'FWHM (")' 'Type of Value' ...
        'Value (W/m²/sr)' 'Uncertainty (%)' 'Modeled value' 'bff based on Halpha' ...
        'Modeled value / geometry based on Halpha'};
else
    indii=0;
end
%

for it=1:nfiles
    Simul(:,it) = ReadSimul(filepaths{it},indSimul);
    if isdisk(it)==1
        % ADDED
        if it==indii
            aj = Simul(:,indii);
            for i=1:length(aj)
                XLSOUTP{i,1} = aj(i);
            end
            XLSOUT = [XLSOUT XLSOUTP];
        end
        %
        [Simul(:,it), bff] = CorrectSimul(CarinaObs,Simul(:,it),0.83/2,0.4/2);
        % ADDED
        if it==indii
            aj = Simul(:,indii);
            for i=1:length(aj)
                XLSOUTP{i,1} = aj(i);
                XLSOUTPP{i,1} = bff(i);
            end
            XLSOUT = [XLSOUT XLSOUTPP XLSOUTP];
        end
        %
        
    else
        % ADDED
        if it==indii
            aj = Simul(:,indii);
            for i=1:length(aj)
                XLSOUTP{i,1} = aj(i);
            end
            XLSOUT = [XLSOUT XLSOUTP];
        end
        %
        [Simul(:,it), bff] = CorrectSimul(CarinaObs,Simul(:,it),9.5/2,3.7/2);
        % ADDED
        if it==indii
            aj = Simul(:,indii);
            for i=1:length(aj)
                XLSOUTP{i,1} = aj(i);
                XLSOUTPP{i,1} = bff(i);
            end
            XLSOUT = [XLSOUT XLSOUTPP XLSOUTP];
        end
        %
    end
    [plothandles, lstr] = AddSimulPlot(X,Simul(:,it),...
        plothandles,lstr,modeldescriptions(it),...
        '''location'',''NorthWest''',...
        mkt,mks,mkc(it,:),mkc(it,:));
    if abs(cell2mat(CarinaObs([7 8],7))-Simul([7 8],it))./...
            cell2mat(CarinaObs([7 8],7)) < ...
            cell2mat(CarinaObs([7 8],8))/100 & correctornot == 1
        [Simul(:,it), bff_misestimationfactor, IndCO] = CorrectCO(CarinaObs,Simul(:,it),0);
        % ADDED
        if it==indii
            aj = Simul(:,indii);
            for i=1:length(aj)
                XLSOUTP{i,1} = aj(i);
                if IndCO(i)==1
                    XLSOUTPP{i,1} = bff_misestimationfactor(i);
                else
                    XLSOUTPP{i,1} = 1;
                end
            end
            XLSOUT = [XLSOUT XLSOUTPP XLSOUTP];
        end
        %
        [plothandles, lstr] = AddSimulPlot(X,Simul(:,it),...
            plothandles,lstr,...
            [modeldescriptions{it} ' - bff corrected'],...
            '''location'',''NorthWest''',...
            'o',mks,mkc(it,:),mkc(it,:));
    end
end

xticklabel = cell(size(CarinaObs,1),1);
for it=1:size(CarinaObs,1)
    xticklabel{it} = [CarinaObs{it,1} char(10) CarinaObs{it,2}];
end
axe = axis;
axe(1:2) = [0 size(CarinaObs,1)+1];
axis(axe);
my_xticklabels(gca,X,xticklabel,'Rotation',90,...
    'HorizontalAlignment','Right','VerticalAlignment','Middle',...
    'fontsize',9);
set(gca,'YScale','log')
set(gca,'fontsize',fs-2)

% % Change legend
% nstrcol = [2 length(cell2mat(strfind(lstr,'isoch'))) ...
% length(cell2mat(strfind(lstr,'isob'))) ...
% length(cell2mat(strfind(lstr,'Env')))];
set(gca,'box','on')
% print('-dpng','-r500','PDR_Isoch.png')
% print('-dpng','-r500','PDR_Isob.png')
% print('-dpng','-r500','PDR_Env.png')

%% Best model
fig2 = figure;
set(fig2,'units','normalized','position',[0 0 1 1])
hold on
filepaths = {...
    [datapath 'CARINA_1e7_Av5_500K_corr_2/lines_light'];...
%     [datapath 'CARINA_5e6_Av5_500K_corr/lines_light'];...
    [datapath 'CARINA_ENV_n15000/lines_light']};
SimulOpt = zeros(size(CarinaObs,1),2);
for it=1:2
    SimulOpt(:,it) = ReadSimul(filepaths{it},indSimul);
end
SimulOpt(:,1) = CorrectSimul(CarinaObs,SimulOpt(:,1),0.83/2,0.4/2);
SimulOpt(:,2) = CorrectSimul(CarinaObs,SimulOpt(:,2),9.5/2,3.7/2);
    
% Plot obs.
indFlux = strcmpi(CarinaObs(:,6),'Flux');
X = find(indFlux);
X = 1.5*X;
mks = 6;
fig2_p1 = errorbar(X,cell2mat(CarinaObs(indFlux,7)),...
    0.01*cell2mat(CarinaObs(indFlux,8)).*cell2mat(CarinaObs(indFlux,7)),...
    'ko','MarkerSize',mks,...
    'MarkerFaceColor','k','MarkerEdgeColor','k');
indSupFlux = strfind(CarinaObs(:,6),'SupFlux');
indSupFlux = ~cellfun(@isempty,indSupFlux);
X = find(indSupFlux);
X = 1.5*X;
fig2_p2 = plot(X,cell2mat(CarinaObs(indSupFlux,7))+...
    0.01*cell2mat(CarinaObs(indSupFlux,8)).*...
    cell2mat(CarinaObs(indSupFlux,7)),...
    'rv','MarkerSize',mks,...
    'MarkerFaceColor','r','MarkerEdgeColor','r');
legend('Observations','Non detection',...
    'location','NorthWest')

% Xtick
X = 1:size(CarinaObs,1);
X = 1.5*X;
xticklabel = cell(size(CarinaObs,1),1);
for it=1:size(CarinaObs,1)
    xticklabel{it} = [CarinaObs{it,1} char(10) CarinaObs{it,2}];
end
axe = axis;
axe(1:2) = [0 X(end)+1];
axis(axe);
my_xticklabels(gca,X,xticklabel,'Rotation',90,...
    'HorizontalAlignment','Right','VerticalAlignment','Middle',...
    'fontsize',8);
set(gca,'YScale','log')
fs = 12;
set(gca,'fontsize',fs)

% plot simul
fig2_plothandles = [fig2_p1 fig2_p2];
fig2_lstr = {'Observations','Upper Limit'};
% [fig2_plothandles, fig2_lstr] = AddSimulPlot(X,SimulOpt(:,1),...
%     fig2_plothandles,fig2_lstr,'Disk, nH = 1e7 cm^{-3}',...
%     '''location'',''NorthWest''',...
%     's',mks,'r','r'); 
SimulOpt(:,1) = CorrectCO(CarinaObs,SimulOpt(:,1),0);
[fig2_plothandles, fig2_lstr] = AddSimulPlot(X,SimulOpt(:,1),...
    fig2_plothandles,fig2_lstr,'Disk (expanded for cold CO lines), nH = 1e7 cm^{-3}',...
    '''location'',''NorthWest''',...
    's',mks,'b','b');
[fig2_plothandles, fig2_lstr] = AddSimulPlot(X,SimulOpt(:,2),...
    fig2_plothandles,fig2_lstr,'Envelope, nH = 1.5e4 cm^{-3}',...
    '''location'',''NorthWest''',...
    's',mks,'m','m');
% [fig2_plothandles, fig2_lstr] = AddSimulPlot(X,sum(SimulOpt,2),...
%     fig2_plothandles,fig2_lstr,'Total',...
%     '''location'',''NorthWest''',...
%     's',mks,'m','m');
axe = axis;
axe(3:4) = [1e-12 1e-5];
axis(axe)

set(gca,'box','on')
% print('-dpng','-r600','bestmodel.png')
clear all
close all
clc

% filepath = '/home/champion/Model/1.4.4/out/CARINA_5e7_Av5_4000K_corr/locem';
filepath = '/home/champion/Model/1.4.4/out/CARINA_1e7_Av5_500K_corr_2/locem';
% filepath = '/home/champion/Model/1.4.4/out/CARINA_5e6_Av5_500K_corr/locem';
% filepath = '/home/champion/Model/1.4.4/out/CARINA_1e6_Av5_500K_corr/locem';
% filepath = '/home/champion/Model/1.4.4/out/CARINA_5e5_Av5_500K/locem';
% filepath = '/home/champion/Model/1.4.4/out/CARINA_isobar_5e9/locem';
% filepath = '/home/champion/Model/1.4.4/out/CARINA_isobar_1e9/locem';
% filepath = '/home/champion/Model/1.4.4/out/CARINA_isobar_6e8/locem';
% filepath = '/home/champion/Model/1.4.4/out/CARINA_isobar_3e8/locem';
data = importdata(filepath);
txt = data.textdata;
txt = txt(2,:);
lstr = txt(2:9);
data = data.data;
Av = data(:,1);
locem = data(:,2:9);
Tg = data(:,10);
Td = data(:,11:end);
ind=1:fix(0.8*length(Av));
Av = Av(ind);
locem = locem(ind,:);
Tg = Tg(ind);
Td = Td(ind,:);
figure
hold on
col = jet(size(locem,2));
for it = 1:size(locem,2)
    plot(Av,locem(:,it),'color',col(it,:))
end
legend(lstr)

[~, indmax] = max(locem);
Avmax = Av(indmax);
Tg_Avmax = Tg(indmax);

figure
plot(Av,Td)
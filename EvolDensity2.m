%% Cleaning workspace
clear all
close all
clc

Lines = {'CO(3-2)';'CO(4-3)';'CO(6-5)';'CO(7-6)';...
    'CO(10-9)';'CO(15-14)';'CO(17-16)';'CO(19-18)';...
    '13CO(3-2)';'[CII]';'[OI]';'HCO+(4-3)';'HCN(4-3)'}; % Line names.
Instru = {'APET';'APET';'APET';'APET|HIFI';'HIFI';...
    'PACS';'PACS';'PACS';'APET';'HIFI';'PACS';'APET';'APET'};
lambda0 = ones(size(Lines))*NaN;
thetab = ones(size(Lines))*NaN;
LineFlux_Kkmpers = ones(size(Lines))*NaN;
Int = ones(length(Lines),3)*NaN;
Int_corr = ones(length(Lines),3)*NaN;

k = 1.38065E-23; % Boltzmann constant in J/K.
c = 299792458; % Light velocity in vaccum in m/s.

%% Correcting observed fluxes
% Data from Sahai et al. (2012)
% Sahai R., Güsten R. and Morris M. R. (2012), Are large, cometary-shaped
% proplyds really evaporating gas globules?. ApJL (761:L21).
ind = strfind(Instru,'APET');
ind = ~cellfun(@isempty,ind);
% ind = strcmpi(Instru,'APET');
lambda0(ind) = [866.963;650.251;433.556;371.650;906.830;840.407;845.665]*1E-6;
thetab(ind) = [17.8;13.1;8.7;7.7;18.6;17.3;17.4]; % Beam sizes (FWHM) in arcsec.
LineFlux_Kkmpers(ind) = [3.5;5.4;7.3;8.1;0.96;0.37;0.21]; % Line fluxes in K.km/s.
% Conversion to SI units.
Int(:,1) = (2E3*k*LineFlux_Kkmpers)./(lambda0.^3); % Int. or line fluxes in W/m²/sr.
Sdisk = pi*(0.83/2)*(0.4/2); % Surface in arcseconds² of the Carina proplyd disk.
Sbeam = pi*(thetab/2).^2; % Surface in arcseconds² of the beam.
bff = Sdisk./Sbeam; % Beam filling factor.
Int_corr(:,1) = Int(:,1)./bff; % Intensity corrected with the beam filling factor.

% Data from our analyses with PACS.
ind = strfind(Instru,'PACS');
ind = ~cellfun(@isempty,ind);
% ind = strcmpi(Instru,'PACS');
lambda0(ind) = [173.631;153.267;137.196;63.184]*1E-6;
Int(ind,2) = [2E-9;2.76E-9;NaN;1.24E-6]; % Line fluxes in W/m²/sr.
Sbeam = ones(length(Lines),1) * 9.4^2; % Pixel of 9.4 by 9.4 arcsec².
bff = Sdisk./Sbeam; % Beam filling factor.
Int_corr(:,2) = Int(:,2)./bff;

% Data from our analyses with HIFI.
ind = strfind(Instru,'HIFI');
ind = ~cellfun(@isempty,ind);
% ind = strcmpi(Instru,'HIFI');
lambda0(ind) = [371.650;260.240;157.68]*1E-6;
f0 = c./lambda0;
Int(ind,3) = [4.79E-10;1.57E-9;1.29E-7]; % Line fluxes in W/m²/sr.
Table = [480 44.3; 640 33.2; 800 26.6; 960 22.2; 1120 19; 1410 15.2;...
    1910 11.2]; % Frequency (GHz) vs FWHM (arcsec.), from HIFI observer manual.
% Calculation of the FWHM (frequency dependent) for each HIFI observation,
% and thus the beam aperture.
% Sbeam = ones(size(Lines))*NaN;
for i = find(ind(:)'==1)
    [~, indi] = min(abs(Table(:,1)-f0(i)*1E-9));
    FWHM = Table(indi,2) * Table(indi,1)/(f0(i)*1E-9); % Because f1/f2 = FWHM2/FWHM1.
    Sbeam(i) = pi * (FWHM/2)^2;
end
bff = Sdisk./Sbeam; % Beam filling factor.
Int_corr(:,3) = Int(:,3)./bff;

%% Simulation
% filepath = '/home/champion/Model/1.4.4/out/CARINA_default_Av10_O10x/lines_light';
% simul_1e5 = ReadSimul(filepath);
% filepath = '/home/champion/Model/1.4.4/out/CARINA_default_Av10_O10x_1e6/lines_light';
% simul_1e6 = ReadSimul(filepath);
% filepath = '/home/champion/Model/1.4.4/out/CARINA_default_Av10_O10x_5e6/lines_light';
% simul_5e6 = ReadSimul(filepath);
filepath = '/home/champion/Model/1.4.4/out/CARINA_1e7_Av5_500K/lines_light';
simul_1e7 = ReadSimul(filepath);
filepath = '/home/champion/Model/1.4.4/out/CARINA_1e7_Av5_500K_corr/lines_light';
simul_1e7_2 = ReadSimul(filepath);
filepath = '/home/champion/Model/1.4.4/out/CARINA_1e7_Av5_500K_corr_2/lines_light';
simul_1e7_3 = ReadSimul(filepath);
% filepath = '/home/champion/Model/1.4.4/out/CARINA_isobar_3e8/lines_light';
% simul_isobar = ReadSimul(filepath);
% filepath = '/home/champion/Model/1.4.4/out/CARINA_isobar_6e8/lines_light';
% simul_isobar2 = ReadSimul(filepath);
% filepath = '/home/champion/Model/1.4.4/out/CARINA_isobar_1e9/lines_light';
% simul_isobar3 = ReadSimul(filepath);

%% Figure
figure
set(gcf,'units','normalized')
set(gcf,'position',[0 0 1 1])

mks = 5;
semilogy(Int_corr(:,1),'o','MarkerSize',mks,'MarkerFaceColor','r',...
    'MarkerEdgeColor','r')
hold on
semilogy(Int_corr(:,2),'s','MarkerSize',mks,'MarkerFaceColor','g',...
    'MarkerEdgeColor','g')
semilogy(Int_corr(:,3),'d','MarkerSize',mks,'MarkerFaceColor','b',...
    'MarkerEdgeColor','b')
xticklabel = cell(size(Lines));
for it=1:length(Lines)
    xticklabel{it} = [Lines{it} char(10) Instru{it}];
end
axe = axis;
axe(1:2) = [0 length(Lines)+1];
axis(axe);
my_xticklabels(gca,1:length(Lines),xticklabel,'Rotation',90,...
    'HorizontalAlignment','Right','VerticalAlignment','Middle',...
    'fontsize',8);
fs = 13;
ylabel('Line intensities (W/m²/sr)','fontsize',fs)
set(gca,'fontsize',fs)
% 
% mks = 2;
% semilogy(simul_1e5,'s','MarkerSize',mks,'MarkerEdgeColor','k')
% mks = 3;
% semilogy(simul_1e6,'s','MarkerSize',mks,'MarkerEdgeColor','k')
% mks = 4;
% semilogy(simul_5e6,'s','MarkerSize',mks,'MarkerEdgeColor','k')
mks = 5;
semilogy(simul_1e7,'s','MarkerSize',mks,'MarkerEdgeColor','k')
mks = 5;
semilogy(simul_1e7_2,'s','MarkerSize',mks,'MarkerEdgeColor','c')
mks = 5;
semilogy(simul_1e7_3,'s','MarkerSize',mks,'MarkerEdgeColor','m')
% mks = 5;
% semilogy(simul_isobar,'s','MarkerSize',mks,'MarkerEdgeColor','m')
% mks = 5;
% semilogy(simul_isobar2,'s','MarkerSize',mks,'MarkerEdgeColor','c')
% mks = 5;
% semilogy(simul_isobar3,'s','MarkerSize',mks,'MarkerEdgeColor','r')

% legend('Corrected APET Obs.','Corrected PACS Obs.',...
%     'Corrected HIFI Obs.','Simulation (1e5)','Simulation (1e6)',...
%     'Simulation (5e6)','Simulation (1e7)','Location','NorthWest')
legend('Corrected APET Obs.','Corrected PACS Obs.',...
    'Corrected HIFI Obs.','Instable simulation','Corrected but instable simulation',...
    'Corrected simulation','Location','NorthWest')

% print('-dpng','-r500','EvolDensity2.png')

%%
figure
IndLines = 1:8;
Eup = [33.2 55.3 116.2 154.87 304.17 663.36 845.61 1049.85]';
intobs_corr = nanmean(Int_corr,2);
% intobs = nanmean(Int,2);
% difint = abs((intobs_corr(IndLines)-simul_1e7(IndLines))./intobs_corr(IndLines));
% plot(Eup,difint,'o')
xlabel('Eup (K)')
ylabel('Relative difference')

bff_factor = intobs_corr(IndLines)./simul_5e6(IndLines);
plot(Eup,bff_factor,'o');
factor = fittype( @(factor0,E0,x) factor0*exp(-x/E0)+1 );
opt = fitoptions(factor);
opt.Lower = [0 0];
opt.Upper = [20 1000];
opt.StartPoint = [8 200];
indnan = find(isnan(bff_factor));
bff_factor(indnan) = [];
Eup(indnan) = [];
fitresult = fit(Eup,bff_factor,factor,opt);
Eupmod = 0:1200;
factormod = factor(fitresult.factor0,fitresult.E0,Eupmod);
hold on
plot(Eupmod,factormod,'r')

fs = 13;
xlabel('Eup (K)','fontsize',fs)
ylabel('Factor of BFF underestimation','fontsize',fs)
set(gca,'fontsize',fs)
% legend('Simulation @ 1e7 cm^{-3} Vs. Obs.','factor0 * exp(-Eup/E0) + 1')
% print('-dpng','-r600','FactorBFF.png')